using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphCollection {
  public class Graph {
    public Dictionary<string, LinkedList<string>> graphDictionary = new Dictionary<string, LinkedList <string>>();

    public string[] getVertex() {
      return graphDictionary.Keys.ToArray();
    }

    public bool addVertex(string vertex) {
      if (!graphDictionary.ContainsKey(vertex)) {
        graphDictionary.Add(vertex, new LinkedList <string> ());
        return true;
      }

      return false;
    }

    public bool removeVertex(string vertex) {
      if (graphDictionary.ContainsKey(vertex)) {
        graphDictionary.Remove(vertex);

        foreach(var key in getVertex()) {
          if (graphDictionary[key].Contains(vertex)) {
            graphDictionary[key].Remove(vertex);
          }
        }
      } else {
        return false;
      }

      return true;
    }

    public void addEdge(string u, string v) {
      if (!graphDictionary.ContainsKey(u)) {
        addVertex(u);
      }

      if (!graphDictionary.ContainsKey(v)) {
        addVertex(v);
      }

      var last = graphDictionary[u].Last;

      if (!graphDictionary[u].Contains(v)) {
        if (last != null) {
          graphDictionary[u].AddAfter(last, v);
        } else {
          graphDictionary[u].AddFirst(v);
        }
      }
    }

    public bool removeEdge(string u, string v) {
      if (graphDictionary.ContainsKey(u) && graphDictionary.ContainsKey(v)) {
        graphDictionary[u].Remove(v);

        if (graphDictionary[v].Contains(u)) {
          graphDictionary[v].Remove(u);
        }

        return true;
      }

      return false;
    }

    public List<Dictionary<string, LinkedList<string>>> getSubGraps() {
      DepthFirstSearch depthFirstSearch = new DepthFirstSearch();

      depthFirstSearch.DFSHelperWithStack(graphDictionary);
      List<Dictionary<string, LinkedList<string>>> subGraph = new List<Dictionary<string, LinkedList<string>>>();
      

      foreach(var i in depthFirstSearch.visited) {
        Dictionary<string, LinkedList<string>> graph = new Dictionary<string, LinkedList<string>>();
        
        foreach(var j in i) {
          graph.Add(j.Value, graphDictionary[j.Value]);
        }

        subGraph.Add(graph);
      }

      return subGraph;
    }

    public bool isStronglyConnected(Dictionary<string, LinkedList<string>> graph = null) {
      DepthFirstSearch depthFirstSearch = new DepthFirstSearch(); 

      graph = graph != null ? graph : graphDictionary;

      foreach(var vertex in graph.Keys.ToArray()) {
        depthFirstSearch.DFSHelperWithStack(
          graph,
          vertex,
          null,
          false
        );

        if (depthFirstSearch.visited[0].Count < graph.Count) {
          return false;
        } else {
          depthFirstSearch.visited.Clear();
        }
      }

      return true;
    }

    public void printScc(string vertex = null) {
      var subGraps = getSubGraps();

      foreach(var subGraph in subGraps) {
        BreadthFirstSearch breadthFirstSearch = new BreadthFirstSearch();
        breadthFirstSearch.BFSHelperWithQueue(subGraph);

        for (int i = 0; i < breadthFirstSearch.visited.Count; i++) {
        int loopCount = 0;

        if (isStronglyConnected(subGraph)) {
          Console.WriteLine("\n\n\tO subgrafo é fortemente conexo");
        } else {
          Console.WriteLine("\n\n\tO subgrafo não é fortemente conexos");
        }
        
        foreach(var j in breadthFirstSearch.visited[i].Keys.ToArray()) {
            if (loopCount == 0) {
              Console.WriteLine();
              Console.ForegroundColor = ConsoleColor.Red;
              Console.Write("\t" + breadthFirstSearch.visited[i][j]);
              Console.ForegroundColor = ConsoleColor.White;
            } else {
              Console.Write(", " + breadthFirstSearch.visited[i][j]);
            }

            loopCount++;
          }
        }        
      }
    }
  }
}