﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphCollection {
  public class BreadthFirstSearch {
    public List<Dictionary<string, string>> visited = new List<Dictionary<string, string>>();
    
    internal void BFS(
      Dictionary<string, LinkedList<string>> graph,
      string vertex = null,
      string lookingFor = null
    ) {
      Console.WriteLine("\n\n\tBFS");

      BFSHelperWithQueue(
        graph,
        vertex,
        lookingFor
      );
      var visitedArray = visited.ToArray();
      
      for (int i = 0; i < visited.Count; i++) {
        int loopCount = 0;
        
        foreach(var j in visited[i].Keys.ToArray()) {
          if (loopCount == 0) {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\t" + visited[i][j]);
            Console.ForegroundColor = ConsoleColor.White;
          } else {
            Console.Write(", " + visited[i][j]);
          }

          loopCount++;
        }
      }
        
      Console.WriteLine();
    }

    public void BFSHelperWithQueue(
      Dictionary<string, LinkedList<string>> graph,
      string vertex = null,
      string lookingFor = null,
      bool navSubGraph = true
    ) {
      if (vertex == null || !graph.ContainsKey(vertex)) {
        var adjVertex = graph.Keys.ToArray();

        if (adjVertex.Length == 0) return;

        vertex = adjVertex[0];
      }

      // marca como visitado
      visited.Add(new Dictionary<string, string>());
      visited[visited.Count - 1].Add(vertex, vertex);
    
      // Queue para BFS
      var queue = new Queue<string>();

      // adicionar o node na fila
      queue.Enqueue(vertex);

      while (queue.Count > 0) {
        var current = queue.Dequeue();

        if (current == lookingFor) return;

        if (graph.ContainsKey(current)) {
          foreach(string child in graph[current].ToArray().Where(v => (visited.Where(v2 => v2.ContainsKey(v)).Count() == 0))) {
            visited[visited.Count - 1].Add(child, child);
            queue.Enqueue(child);
          } 
        } 

        if (queue.Count == 0 && visited.Count < graph.Count && navSubGraph) {
          foreach(string v in graph.Keys.ToArray().Where(v => (visited.Where(v2 => v2.ContainsKey(v)).Count() == 0))) {
            BFSHelperWithQueue(
              graph,
              v,
              lookingFor
            );
          }
        }
      }
    }
  }
}
