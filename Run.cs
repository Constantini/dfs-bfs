using System;
using GraphCollection;
using Print;

namespace Core {
  class Run {
    static Graph graph;
    static BreadthFirstSearch breadthFirstSearch;
    static DepthFirstSearch depthFirstSearch;

    static void Main(string[] args) {
      graph = new Graph();
      depthFirstSearch = new DepthFirstSearch();
      breadthFirstSearch = new BreadthFirstSearch();
      
      for (int i = 0;; i++) {
        string command = "";
        
        if (i == 0) {
          Console.Clear();
          help();
        } else {
          Console.Clear();
          Console.Write("\t$ ");
          command = (Console.ReadLine()).ToLower().Replace("$", "").Trim();
        }

        if (command.Contains("add") && command.Contains("-v")) {
          command = command.Replace("add", "").Trim();
          command = command.Replace("-v", "").Trim();

          var vertex = command.Split(' ');

          if (vertex.Length < 1) continue;

          graph.addVertex(vertex[0]);
        } else if (command.Contains("add") && command.Contains("-a")) {
          command = command.Replace("add", "").Trim();
          command = command.Replace("-a", "").Trim();

          var edges = command.Split(' ');

          if (edges.Length < 2) continue;

          graph.addEdge(edges[0], edges[1]);
        } else if (command.Contains("remove") && command.Contains("-v")) {
          command = command.Replace("remove", "").Trim();
          command = command.Replace("-v", "").Trim();

          var vertex = command.Split(' ');

          if (vertex.Length < 1) continue;

          graph.removeVertex(vertex[0]);
        } else if (command.Contains("remove") && command.Contains("-a")) {
          command = command.Replace("remove", "").Trim();
          command = command.Replace("-a", "").Trim();

          var edges = command.Split(' ');

          if (edges.Length < 2) continue;

          graph.removeEdge(edges[0], edges[1]);
        } else if (command.Contains("print") || command.Contains("-p")) {
          PrintGraph.Write(graph.graphDictionary);
        } else if (command.Contains("-s") || command.Contains("search")) {
          command = command.Replace("-s", "").Replace("search", "").Trim();

          var vertex = command.Split(' ')[0];

          string lookingFor = null;

          if (command.Length > 1) {
            lookingFor = command.Split(' ')[1].Trim();
          }

          depthFirstSearch.DFS(graph.graphDictionary, vertex, lookingFor);
          breadthFirstSearch.BFS(graph.graphDictionary, vertex, lookingFor);
          PrintGraph.Write(graph.graphDictionary);          
        } else if (command.Contains("-c")) {
          var vertex = command.Replace("-c", "").Trim().Split(' ')[0].Trim();
          
          depthFirstSearch.DFS(graph.graphDictionary, vertex, null);
          breadthFirstSearch.BFS(graph.graphDictionary, vertex, null);

          depthFirstSearch.visited.Clear();
          breadthFirstSearch.visited.Clear();
          
          var isStronglyConnected = graph.isStronglyConnected();

          if (isStronglyConnected) {
            Console.WriteLine("\n\tO grafo é conexo");
          } else {
            Console.WriteLine("\n\tO grafo não é conexo");
          }

          PrintGraph.Write(graph.graphDictionary);
          Console.WriteLine();

          if (!isStronglyConnected) graph.printScc();
        } else if (command.Contains("help") || command.Contains("-h")) {
          help();
        } else {
          help();
        }

        Console.WriteLine();
        depthFirstSearch.visited.Clear();
        breadthFirstSearch.visited.Clear();
        Console.Write("\n\tENTER para continuar. ");
        Console.ReadLine();
      }
    }

    private static void help() {
      Console.Clear();
      Console.WriteLine("\tComandos para modificar o grafo");
      Console.WriteLine();
      
      Console.WriteLine("\t-h ou help ajuda");
      Console.WriteLine("\tadd -v (nome da vértice): adiciona vértice");
      Console.WriteLine("\tadd -a (nome da vértice 1) (nome da vértice 2): adiciona arco/aresta");
      Console.WriteLine("\tremove -v (nome da vértice): remove vértice");
      Console.WriteLine("\tremove -a (nome da vértice 1) (nome da vértice 2): remove arco/aresta");
      Console.WriteLine("\t-c (-a opcional vértice inicial): verifica se o grafo é conexo ou não, E caso não seja conexo, identifica os sub-grafos fortemente conexos do grafo");
    }
  }
}