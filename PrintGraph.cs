using System;
using System.Collections.Generic;
using System.Linq;

namespace Print {
  public class PrintGraph {
    public static void Write(Dictionary<string, LinkedList<string>> graph) {
      Console.WriteLine();
       
       // ordena array  
       graph =  graph.OrderBy(a => a.Key).ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);

      var count = graph.Keys.Count + 1;
      var keyArray = graph.Keys.ToArray();

      string[, ] matrix = new string[count, count];

      for (var i = 1; i < count; i++) {
        matrix[i, 0] = keyArray[i - 1];
        matrix[0, i] = keyArray[i - 1];
      }

      for (var i = 1; i < count; i++) {
        for (var j = 1; j < count; j++) {
          var line = matrix[i, 0];
          var column = matrix[0, j];

          if (graph[line].Contains(column)) {
            matrix[i, j] = "1";
          } else {
            matrix[i, j] = "0";
          }
        }
      }

      WriteArray(matrix);
    }

    private static void WriteArray(string[, ] matrix) {
      int rowLength = matrix.GetLength(0);
      int colLength = matrix.GetLength(1);

      for (int i = 0; i < rowLength; i++) {
        for (int j = 0; j < colLength; j++) {
          if (matrix[i, j] == "1" && i != 0 && j != 0) {
            Console.ForegroundColor = ConsoleColor.Green;
          }

          if (i == 0 && j == 0) {
            Console.Write(string.Format("\t {0} ", matrix[i, j]));
          } else {
            Console.Write(string.Format("\t{0} ", matrix[i, j]));
          }

          Console.ForegroundColor = ConsoleColor.White;
        }

        Console.WriteLine();
      }
    }
  }
}
