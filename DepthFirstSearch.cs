﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphCollection {
  class DepthFirstSearch {
    private Stack<string> stack = new Stack<string>();

    public List<Dictionary<string, string>> visited = new List<Dictionary<string, string>>();

    internal void DFS(
      Dictionary<string, LinkedList<string>> graph,
      string vertex = null,
      string lookingFor = null
    ) {
      Console.WriteLine("\n\n\tDFS");

      DFSHelperWithStack(
        graph,
        vertex,
        lookingFor
      );
      
      for (int i = 0; i < visited.Count; i++) {
        int loopCount = 0;
        
        foreach(var j in visited[i].Keys.ToArray()) {
          if (loopCount == 0) {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\t" + visited[i][j]);
            Console.ForegroundColor = ConsoleColor.White;
          } else {
            Console.Write(", " + visited[i][j]);
          }

          loopCount++;
        }
      }
        
      Console.WriteLine();
    }

    public void DFSHelperWithStack(
      Dictionary<string, LinkedList<string>> graph,
      string vertex = null, string lookingFor = null,
      bool navSubGraph = true,
      bool isSubGraph = false
    ) {
      if (vertex == null || !graph.ContainsKey(vertex)) {
        var adjVertex = graph.Keys.ToArray();

        if (adjVertex.Length == 0) return;

        vertex = adjVertex[0];
      }

      if (visited.Where(v => v.ContainsKey(vertex)).Count() == 0) {
        if (isSubGraph) {
          visited.Add(new Dictionary<string, string>());
        } else if (visited.Count < 1) {
          visited.Add(new Dictionary<string, string>());
        } 

        visited[visited.Count - 1].Add(vertex, vertex);
        stack.Push(vertex);
      }

      if (lookingFor == vertex) return;

      foreach(var child in graph[vertex].ToArray().Where(v => (visited.Where(v2 => v2.ContainsKey(v)).Count() == 0))) {
        DFSHelperWithStack(
          graph,
          child,
          lookingFor,
          navSubGraph,
          false
        );
        
        return;
      }

      if (stack.Count != 0) {
        DFSHelperWithStack(
          graph,
          stack.Pop(),
          lookingFor,
          navSubGraph,
          false
        );
      } else {
        foreach(string v in graph.Keys.ToArray().Where(v => (visited.Where(v2 => v2.ContainsKey(v)).Count() == 0))) {
          DFSHelperWithStack(graph, v, lookingFor, navSubGraph, true);
        }
      }
    }
  }
}
